#!/bin/sh

TMP=/tmp/nepifarewi-count
if [ ! -f $TMP ]; then echo "0">$TMP; fi

restart_wifi()
{
    logger -t "nepifarewi" -p 5 "$1, restart wifi"
    OFF_COUNT=$(cat $TMP)
    echo "$(($OFF_COUNT + 1))" > $TMP
    wifi
}

[ "$(uci -q get nepifarewi.settings.enabled)" = '0' ] && exit 0

NMACS=`batctl n|grep mesh0|cut -c 1-17`
COUNT=`echo $NMACS|wc -w`

[[ $COUNT -eq 0 ]] && [ "$(uci -q get nepifarewi.settings.force)" = '1' ] && (restart_wifi "No neighbors"; exit 1)

ERRC=0
for NMAC in $NMACS; do
    EB=`echo $NMAC|cut -c 17-`
    batctl p -c1 `echo $NMAC|cut -c1-16`$((EB+2)) >/dev/null || ERRC=$((ERRC+1))
done
[[ $ERRC -eq $COUNT ]] && (restart_wifi "No pong from all neighbors"; exit 2)
echo All good. Neighbors: $COUNT Replies: $((COUNT - ERRC))
