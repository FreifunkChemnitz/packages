#!/bin/sh

safety_exit() {
	echo $1, exiting with error code 2
	exit 2
}
pgrep -f autoupdater >/dev/null && safety_exit 'autoupdater running'
UT=$(sed 's/\..*//g' /proc/uptime)
FIRST="$(uci -q get wodca.settings.first)"
: ${FIRST:=5}
[ $UT -gt $((FIRST*60)) ] || safety_exit 'too early'
[ $(find /var/run -name hostapd-phy* | wc -l) -gt 0 ] || safety_exit 'no hostapd-phy*'

PREFIX="$(uci -q get wodca.settings.prefix)"
: ${PREFIX:='FF_Offline_'}

if [ "$(uci -q get wodca.settings.enabled)" = '0' ]; then 
	DISABLED='1'
else
	DISABLED='0'
fi

SETTINGS_SUFFIX="$(uci -q get wodca.settings.suffix)"
: ${SETTINGS_SUFFIX:='nodename'}

if [ $SETTINGS_SUFFIX = 'nodename' ]; then
	SUFFIX="$(uname -n)"
	if [ ${#SUFFIX} -gt $((30 - ${#PREFIX})) ]; then
		HALF=$(( (28 - ${#PREFIX} ) / 2 ))
		SKIP=$(( ${#SUFFIX} - $HALF ))
		SUFFIX=${SUFFIX:0:$HALF}...${SUFFIX:$SKIP:${#SUFFIX}}
	fi
elif [ $SETTINGS_SUFFIX = 'mac' ]; then
	SUFFIX="$(uci -q get network.bat0.macaddr | /bin/sed 's/://g')"
else
	SUFFIX=''
fi

OFFLINE_SSID="$PREFIX$SUFFIX"

CLIENT_RADIOS=$(uci show wireless | grep network=\\\'client\\\' | sed 's/.*\(client_radio[0-9]\+\).*/\1/')
for CLIENT_RADIO in $CLIENT_RADIOS ; do
	if [ $(uci get wireless.$CLIENT_RADIO.disabled) -eq 1 ]; then
		CLIENT_RADIOS=${CLIENT_RADIOS/$CLIENT_RADIO/}
	fi
done

TMP=/tmp/wodca-count
if [ ! -f $TMP ]; then echo "0">$TMP; fi
OFF_COUNT=$(cat $TMP)

check-online
CHECK=$?

find_phy() {
	for PHY in $(ls /var/run/hostapd-phy*); do
		if [ $(uci get wireless.$1.ifname) = $(grep ^interface= $PHY|cut -d= -f2) ] ; then
			echo $PHY
			break
		fi
	done
}

HUP_NEEDED=0
if [ "$CHECK" -eq 0 ] || [ "$DISABLED" = '1' ]; then
	echo "node is online"
	for CLIENT_RADIO in $CLIENT_RADIOS ; do
		HOSTAPD=$(find_phy $CLIENT_RADIO)
		if [ -f $HOSTAPD ]; then
			CURRENT_SSID=$(grep ^ssid= $HOSTAPD | cut -d= -f2)
			ONLINE_SSID=$(uci get wireless.$CLIENT_RADIO.ssid)
			if [ $CURRENT_SSID = $ONLINE_SSID ]; then
				echo $CLIENT_RADIO: SSID $CURRENT_SSID is correct, nothing to do
			else
				logger -s -t wodca -p 5 "$CLIENT_RADIO: SSID is $CURRENT_SSID, change to $ONLINE_SSID"
				sed -i "s/^ssid=$CURRENT_SSID/ssid=$ONLINE_SSID/" $HOSTAPD
				HUP_NEEDED=1
			fi
		else
			echo Cannot find hostapd config for $CLIENT_RADIO
		fi
	done
elif [ "$CHECK" -gt 0 ]; then
	echo "node is considered offline"
	for CLIENT_RADIO in $CLIENT_RADIOS ; do
		HOSTAPD=$(find_phy $CLIENT_RADIO)
		if [ -f $HOSTAPD ]; then
			CURRENT_SSID=$(grep ^ssid= $HOSTAPD | cut -d= -f2)
			if [ $CURRENT_SSID = $OFFLINE_SSID ]; then
				echo $CLIENT_RADIO: SSID $CURRENT_SSID is correct, nothing to do
			else
				logger -s -t wodca -p 5 "$CLIENT_RADIO: $OFF_COUNT times offline, SSID is $CURRENT_SSID, change to $OFFLINE_SSID"
				sed -i "s/^ssid=$CURRENT_SSID/ssid=$OFFLINE_SSID/" $HOSTAPD
				HUP_NEEDED=1
			fi
		else
			echo Cannot find hostapd config for $CLIENT_RADIO
		fi
	done
	echo "$(($OFF_COUNT + 1))">$TMP
fi

if [ $HUP_NEEDED -eq 1 ]; then
	killall -HUP hostapd
fi
