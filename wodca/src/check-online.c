/*
   Copyright (c) 2018, Thomas Lauer <lauer_thomas@freenet.de>
   All rights reserved.

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
   2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
   DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
   FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
   SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
   CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
   OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
   */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "batadv-netlink.h"

#define DEFAULT_TIMEOUT "15"
#define INTERFACE "bat0"

void usage() {
    puts("Usage: check-online [timeout]\n");
    puts("  timeout is compared to last seen value of gateway\n");
    puts("  unit of timeout is seconds, default is " DEFAULT_TIMEOUT "\n");
}

struct my_query_opts {
    uint8_t gateway[6];
    uint32_t lastseen;
    struct batadv_nlquery_opts query_opts;
};

static const enum batadv_nl_attrs gw_list_attrs[] = {
    BATADV_ATTR_ORIG_ADDRESS,
};

static int
gw_list_cb(struct nl_msg *msg, void *arg)
{
    struct nlattr *attrs[BATADV_ATTR_MAX+1];
    struct nlmsghdr *nlh = nlmsg_hdr(msg);
    struct batadv_nlquery_opts *query_opts = arg;
    struct genlmsghdr *ghdr;
    uint8_t *orig;
    struct my_query_opts *opts;

    opts = container_of(query_opts, struct my_query_opts, query_opts);

    if (!genlmsg_valid_hdr(nlh, 0)) {
	return NL_OK;
    }

    ghdr = nlmsg_data(nlh);

    if (ghdr->cmd != BATADV_CMD_GET_GATEWAYS) {
	return NL_OK;
    }

    if (nla_parse(attrs, BATADV_ATTR_MAX, genlmsg_attrdata(ghdr, 0),
		  genlmsg_len(ghdr), batadv_netlink_policy))
    {
	return NL_OK;
    }

    if (batadv_nl_missing_attrs(attrs, gw_list_attrs,
      				ARRAY_SIZE(gw_list_attrs)))
    {
	return NL_OK;
    }

    if (!attrs[BATADV_ATTR_FLAG_BEST]) {
	return NL_OK;
    }

    orig = nla_data(attrs[BATADV_ATTR_ORIG_ADDRESS]);

    memcpy(&opts->gateway, orig, 6);

    return NL_STOP;
}

static void
get_gateway(struct my_query_opts *opts)
{
    opts->query_opts.err = 0;
    batadv_nl_query_common(INTERFACE, BATADV_CMD_GET_GATEWAYS,
			   gw_list_cb, NLM_F_DUMP,
			   &opts->query_opts);
}

static const enum batadv_nl_attrs orig_list_attrs[] = {
    BATADV_ATTR_ORIG_ADDRESS,
    BATADV_ATTR_LAST_SEEN_MSECS,
};

static int
orig_list_cb(struct nl_msg *msg, void *arg)
{
    struct nlattr *attrs[BATADV_ATTR_MAX+1];
    struct nlmsghdr *nlh = nlmsg_hdr(msg);
    struct batadv_nlquery_opts *query_opts = arg;
    struct genlmsghdr *ghdr;
    uint8_t *orig;
    uint32_t lastseen;
    struct my_query_opts *opts;

    opts = container_of(query_opts, struct my_query_opts, query_opts);

    if (!genlmsg_valid_hdr(nlh, 0)) {
	return NL_OK;
    }

    ghdr = nlmsg_data(nlh);

    if (ghdr->cmd != BATADV_CMD_GET_ORIGINATORS) {
	return NL_OK;
    }

    if (nla_parse(attrs, BATADV_ATTR_MAX, genlmsg_attrdata(ghdr, 0),
		  genlmsg_len(ghdr), batadv_netlink_policy))
    {
	return NL_OK;
    }

    if (batadv_nl_missing_attrs(attrs, orig_list_attrs,
      				ARRAY_SIZE(orig_list_attrs)))
    {
	return NL_OK;
    }

    if (!attrs[BATADV_ATTR_FLAG_BEST]) {
	return NL_OK;
    }

    orig = nla_data(attrs[BATADV_ATTR_ORIG_ADDRESS]);
    lastseen = nla_get_u32(attrs[BATADV_ATTR_LAST_SEEN_MSECS]);

    if (memcmp(orig, opts->gateway, sizeof(opts->gateway)) != 0) {
	return NL_OK;
    }

    opts->lastseen = lastseen;

    return NL_STOP;
}

static void
get_last_seen(struct my_query_opts *opts)
{
    opts->query_opts.err = 0;
    batadv_nl_query_common(INTERFACE,
			   BATADV_CMD_GET_ORIGINATORS,
			   orig_list_cb, NLM_F_DUMP, &opts->query_opts);
}

int
main(int argc, char **argv)
{
    char *tout_string;
    int timeout;
    struct my_query_opts opts;

    if (argc > 2) {
	usage();
	exit(2);
    }

    tout_string = argc < 2 ? DEFAULT_TIMEOUT : argv[1];
    timeout = atoi(tout_string);

    if (timeout < 0) {
	printf("A negative timeout (%d or '%s') is not supported. Cannot anticipate the future.\n",
	       timeout, tout_string);
	exit(2);
    }

    memset(opts.gateway, 0, sizeof(opts.gateway));
    get_gateway(&opts);

    if (!memcmp(opts.gateway, "\x00\x00\x00\x00\x00\x00", sizeof(opts.gateway))) {
	puts("No gateway found.\n");
	exit(1);
    }

    opts.lastseen = -1;
    get_last_seen(&opts);

    if (opts.lastseen == -1) {
	printf("Gateway %02x:%02x:%02x:%02x:%02x:%02x not in originators list.\n",
	       opts.gateway[0], opts.gateway[1], opts.gateway[2], 
	       opts.gateway[3], opts.gateway[4], opts.gateway[5]);
	exit(1);
    }

    if (opts.lastseen / 1000 > timeout) {
	printf("Gateway %02x:%02x:%02x:%02x:%02x:%02x timed out: %d > %d\n",
	       opts.gateway[0], opts.gateway[1], opts.gateway[2], 
	       opts.gateway[3], opts.gateway[4], opts.gateway[5],
	       opts.lastseen / 1000, timeout);
	exit(1);
    }

    return 0;
}
